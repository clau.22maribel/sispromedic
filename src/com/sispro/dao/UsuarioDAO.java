package com.sispro.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import com.sispro.conexion.Conexion;
import com.sispro.modelo.Perfil;
import com.sispro.modelo.Usuario;

public class UsuarioDAO {
	
	private Connection connection ;
	private PreparedStatement statement ;
	private boolean estadoOperacion ;

	//guardar usuario
	public boolean guardarUsuario(Usuario usuario) throws SQLException {
		String sql = null ;				//consulta sql vacia
		estadoOperacion = false ;		// es false para decirle que no se ha realizado una consulta a la BD
		connection = obtenerConexion();	//obtiene la conecci
		
		try {
			
			connection.setAutoCommit(false);
			sql = "INSERT INTO seg_usuarios (usu_id, per_id, usu_cedula, usu_nombres, usu_apellidos, "
					+ "usu_direccion, usu_celular, usu_email, usu_usuario, usu_clave, usu_estado) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)";
			statement = connection.prepareStatement(sql);
			
			statement.setString(1, null);
			statement.setInt(2, usuario.getPer_id());
			statement.setString(3, usuario.getUsu_cedula());
			statement.setString(4, usuario.getUsu_nombres());
			statement.setString(5, usuario.getUsu_apellidos());
			statement.setString(6, usuario.getUsu_direccion());
			statement.setString(7, usuario.getUsu_celular());
			statement.setString(8, usuario.getUsu_email());
			statement.setString(9, usuario.getUsu_usuario());
			statement.setString(10, usuario.getUsu_clave());
			statement.setString(11, usuario.getUsu_estado());
			
			estadoOperacion = statement.executeUpdate() > 0;
			
			connection.commit();
			statement.close();
			connection.close();
			
		} catch (SQLException e) {
			
			connection.rollback();
			e.printStackTrace();
		}
		
		return estadoOperacion;
	}
	
	
	//editar usuario
	public boolean editarUsuario(Usuario usuario) throws SQLException {
		String sql = null ;				//consulta sql vacia
		estadoOperacion = false ;		// es false para decirle que no se ha realizado una consulta a la BD
		connection = obtenerConexion();	//obtiene la conecci
		
		try {
			connection.setAutoCommit(false);
			sql = "UPDATE seg_usuarios SET per_id=? , usu_cedula=? , usu_nombres=? , usu_apellidos=? ,"
					+ "usu_direccion=? , usu_celular=? , usu_email=? , usu_usuario=? , usu_clave=? , usu_estado=? WHERE usu_id=?";
			statement = connection.prepareStatement(sql);
			
			statement.setInt(1, usuario.getPer_id());
			statement.setString(2, usuario.getUsu_cedula());
			statement.setString(3, usuario.getUsu_nombres());
			statement.setString(4, usuario.getUsu_apellidos());
			statement.setString(5, usuario.getUsu_direccion());
			statement.setString(6, usuario.getUsu_celular());
			statement.setString(7, usuario.getUsu_email());
			statement.setString(8, usuario.getUsu_usuario());
			statement.setString(9, usuario.getUsu_clave());
			statement.setString(10, usuario.getUsu_estado());
			statement.setInt(11, usuario.getUsu_id());
			
			estadoOperacion = statement.executeUpdate() > 0;
			connection.commit();
			statement.close();
			connection.close();
			
		} catch (SQLException e) {
			
			connection.rollback();
			e.printStackTrace();
		}
		
		return estadoOperacion;
	}
	
	//eliminar un usuario
	public boolean eliminarUsuario(Usuario usuario) throws SQLException {
		String sql = null ;				//consulta sql vacia
		estadoOperacion = false ;		// es false para decirle que no se ha realizado una consulta a la BD
		connection = obtenerConexion();	//obtiene la conecci
		
		try {
			connection.setAutoCommit(false);
			sql = "UPDATE seg_usuarios SET usu_estado=? WHERE usu_id=?";
			statement = connection.prepareStatement(sql);
			
			statement.setString(1, usuario.getUsu_estado());
			statement.setInt(2, usuario.getUsu_id());
			
			estadoOperacion = statement.executeUpdate() > 0;
			connection.commit();
			statement.close();
			connection.close();
			
		} catch (SQLException e) {
			
			connection.rollback();
			e.printStackTrace();
		}
		
		return estadoOperacion;
	}
	
	
	//listar usuarios
	public List<Usuario> listarUsuarios() throws SQLException {
		ResultSet resulSet = null ;
		List<Usuario> listaUsuario = new ArrayList<>();
		
		String sql = null ;				//consulta sql vacia
		estadoOperacion = false ;		// es false para decirle que no se ha realizado una consulta a la BD
		connection = obtenerConexion();	//obtiene la conecci
		
		try {
			
			sql = "SELECT * FROM seg_usuarios" ;
			statement = connection.prepareStatement(sql);
			resulSet = statement.executeQuery(sql);
			
			while (resulSet.next()) {
				Usuario usu1 = new Usuario();
				
				usu1.setUsu_id(resulSet.getInt(1));
				usu1.setPer_id(resulSet.getInt(2));
				usu1.setUsu_cedula(resulSet.getString(3));
				usu1.setUsu_nombres(resulSet.getString(4));
				usu1.setUsu_apellidos(resulSet.getString(5));
				usu1.setUsu_direccion(resulSet.getString(6));
				usu1.setUsu_celular(resulSet.getString(7));
				usu1.setUsu_email(resulSet.getString(8));
				usu1.setUsu_usuario(resulSet.getString(9));
				usu1.setUsu_clave(resulSet.getString(10));
				usu1.setUsu_estado(resulSet.getString(11));
				
				
				listaUsuario.add(usu1);
			}
			
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		return listaUsuario;
	}
	
	// obtener un solo usuario
	public Usuario obtenerUsuario(int idUsuario) throws SQLException {
		
		ResultSet resulSet = null ;
		Usuario usu1 = new Usuario();
		
		String sql = null ;				//consulta sql vacia
		estadoOperacion = false ;		// es false para decirle que no se ha realizado una consulta a la BD
		connection = obtenerConexion();	//obtiene la conecci
		
		try {
			
			sql = "SELECT * FROM seg_usuarios WHERE usu_id=?" ;
			statement = connection.prepareStatement(sql);
			statement.setInt(1, idUsuario);
			
			resulSet = statement.executeQuery();
			
			if (resulSet.next()) {
				
				usu1.setUsu_id(resulSet.getInt(1));
				usu1.setPer_id(resulSet.getInt(2));
				usu1.setUsu_cedula(resulSet.getString(3));
				usu1.setUsu_nombres(resulSet.getString(4));
				usu1.setUsu_apellidos(resulSet.getString(5));
				usu1.setUsu_direccion(resulSet.getString(6));
				usu1.setUsu_celular(resulSet.getString(7));
				usu1.setUsu_email(resulSet.getString(8));
				usu1.setUsu_usuario(resulSet.getString(9));
				usu1.setUsu_clave(resulSet.getString(10));
				usu1.setUsu_estado(resulSet.getString(11));
				
			}
			
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		return usu1;
	}
	
	//obtener conexion pool
	private Connection obtenerConexion() throws SQLException {
		return Conexion.getConnection();
	}
	
	//listar perfiles
	public List<Perfil> listarPerfiles() throws SQLException {
		ResultSet resulSet = null ;
		List<Perfil> listaPerfil = new ArrayList<>();
		
		String sql = null ;				//consulta sql vacia
		estadoOperacion = false ;		// es false para decirle que no se ha realizado una consulta a la BD
		connection = obtenerConexion();	//obtiene la conecci
		
		try {
			
			sql = "SELECT * FROM seg_perfil" ;
			statement = connection.prepareStatement(sql);
			resulSet = statement.executeQuery(sql);
			
			while (resulSet.next()) {
				Perfil per = new Perfil();
				per.setPer_id(resulSet.getInt(1));
				per.setPer_nombre(resulSet.getString(2));
				per.setPer_descripcion(resulSet.getString(3));
				per.setPer_estado(resulSet.getString(4));
				
				listaPerfil.add(per);
			} 
			
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		return listaPerfil;
	}
	
	// obtener un solo usuario
		public Usuario autenticarUsuario(String Ausuario, String Aclave) throws SQLException {
			
			ResultSet resulSet = null ;
			Usuario usu1 = new Usuario();
			
			String sql = null ;				//consulta sql vacia
			estadoOperacion = false ;		// es false para decirle que no se ha realizado una consulta a la BD
			connection = obtenerConexion();	//obtiene la conecci
			
			try {
				
				sql = "SELECT * FROM seg_usuarios WHERE usu_usuario=? AND usu_clave=?" ;
				statement = connection.prepareStatement(sql);
				statement.setString(1, Ausuario);
				statement.setString(2, Aclave);
				
				System.out.println("estamos en USUARIO DAO : " + Ausuario + "  " + Aclave);
				resulSet = statement.executeQuery();
				
				if (resulSet.next()) {
					
					usu1.setUsu_id(resulSet.getInt(1));
					usu1.setPer_id(resulSet.getInt(2));
					usu1.setUsu_cedula(resulSet.getString(3));
					usu1.setUsu_nombres(resulSet.getString(4));
					usu1.setUsu_apellidos(resulSet.getString(5));
					usu1.setUsu_direccion(resulSet.getString(6));
					usu1.setUsu_celular(resulSet.getString(7));
					usu1.setUsu_email(resulSet.getString(8));
					usu1.setUsu_usuario(resulSet.getString(9));
					usu1.setUsu_clave(resulSet.getString(10));
					usu1.setUsu_estado(resulSet.getString(11));
					
				}
				
			} catch (SQLException e) {
				e.printStackTrace();
			}
			
			return usu1;
		}
		
	
	
}
