package com.sispro.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import com.sispro.conexion.Conexion;
import com.sispro.modelo.DetParametrica;
import com.sispro.modelo.Paciente;
import com.sispro.modelo.valoracionMedica;

public class PacienteDAO {
	
	private Connection connection ;				// empieza la coneccion
	private PreparedStatement statement ;		//genera el canal de coneccion para enviar las consultas SQL
	private boolean estadoOperacion ;			//determina cuando se hace una consulta SQL

	//METODO GUARDAR PACIENTE DONANTE
	public boolean guardarDonante(Paciente paciente) throws SQLException {
		String sql = null ;				//consulta sql vacia
		estadoOperacion = false ;		// es false para decirle que no se ha realizado una consulta a la BD
		connection = obtenerConexion();	//obtiene la coneccion
		
		try {
			
			connection.setAutoCommit(false);		//
			// consulta sql que se va a realizar
			sql = "INSERT INTO paciente (pa_id, pa_cedula, pa_extranjero, pa_nombres, pa_apellidos, pa_edad, pa_sexo, pa_estadoCivil, pa_provincia, pa_canton,"
					+ "pa_direccion, pa_email, pa_tipo, pa_celular, pa_checkAlergia, pa_descripcionAlergia, pa_checkDiscapacidad, pa_descripcionDiscapacidad, pa_certificado,"
					+ "pa_seleccionado, pa_estado) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
			statement = connection.prepareStatement(sql);			//prepara la sentencia SQL hacia la Base de datos
			
			statement.setString(1, null);			// el id es autoincremental, asi que es null(valor por defecto que se genera)
			statement.setString(2, paciente.getPa_cedula());
			statement.setInt(3, paciente.getPa_extranjero());
			statement.setString(4, paciente.getPa_nombres());
			statement.setString(5, paciente.getPa_apellidos());
			statement.setInt(6, paciente.getPa_edad());
			statement.setInt(7, paciente.getPa_sexo());
			statement.setInt(8, paciente.getPa_estadoCivil());
			statement.setInt(9, paciente.getPa_provincia());
			statement.setString(10, paciente.getPa_canton());
			statement.setString(11, paciente.getPa_direccion());
			statement.setString(12, paciente.getPa_email());
			statement.setInt(13, paciente.getPa_tipo());
			statement.setString(14, paciente.getPa_celular());
			statement.setInt(15, paciente.getPa_checkAlergia());
			statement.setString(16, paciente.getPa_descripcionAlergia());
			statement.setInt(17, paciente.getPa_checkDiscapacidad());
			statement.setString(18, paciente.getPa_descripcionDiscapacidad());
			statement.setBlob(19, paciente.getPa_certificado());
			statement.setInt(20, paciente.getPa_seleccionado());
			statement.setString(21, paciente.getPa_estado());
			
			estadoOperacion = statement.executeUpdate() > 0;			// significa que se realiz� la consulta exitosamente	
			
			connection.commit();
			statement.close();
			connection.close();
			
		} catch (SQLException e) {
			
			connection.rollback();		//rollback se usa cuando la consulta falla, y no se guardar , restablece antes del error
			e.printStackTrace();
		}
		
		return estadoOperacion;
	}
	
	//guardar paciente contagiado
		public boolean guardarContagiado(Paciente paciente) throws SQLException {
			String sql = null ;				//consulta sql vacia
			estadoOperacion = false ;		// es false para decirle que no se ha realizado una consulta a la BD
			connection = obtenerConexion();	//obtiene la coneccion
			
			try {
				
				connection.setAutoCommit(false);
				sql = "INSERT INTO paciente (pa_id, pa_cedula, pa_extranjero, pa_nombres, pa_apellidos, pa_edad, pa_sexo, pa_estadoCivil, pa_provincia, pa_canton,"
						+ "pa_direccion, pa_email, pa_tipo, pa_celular, pa_checkAlergia, pa_descripcionAlergia, pa_checkDiscapacidad, pa_descripcionDiscapacidad, pa_certificado,"
						+ "pa_seleccionado, pa_estado) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
				statement = connection.prepareStatement(sql);
				
				statement.setString(1, null);
				statement.setString(2, paciente.getPa_cedula());
				statement.setInt(3, paciente.getPa_extranjero());
				statement.setString(4, paciente.getPa_nombres());
				statement.setString(5, paciente.getPa_apellidos());
				statement.setInt(6, paciente.getPa_edad());
				statement.setInt(7, paciente.getPa_sexo());
				statement.setInt(8, paciente.getPa_estadoCivil());
				statement.setInt(9, paciente.getPa_provincia());
				statement.setString(10, paciente.getPa_canton());
				statement.setString(11, paciente.getPa_direccion());
				statement.setString(12, paciente.getPa_email());
				statement.setInt(13, paciente.getPa_tipo());
				statement.setString(14, paciente.getPa_celular());
				statement.setInt(15, paciente.getPa_checkAlergia());
				statement.setString(16, paciente.getPa_descripcionAlergia());
				statement.setInt(17, paciente.getPa_checkDiscapacidad());
				statement.setString(18, paciente.getPa_descripcionDiscapacidad());
				statement.setBlob(19, paciente.getPa_certificado());
				statement.setInt(20, paciente.getPa_seleccionado());
				statement.setString(21, paciente.getPa_estado());
				
				estadoOperacion = statement.executeUpdate() > 0;
				
				connection.commit();
				statement.close();
				connection.close();
				
			} catch (SQLException e) {
				
				connection.rollback();
				e.printStackTrace();
			}
			
			return estadoOperacion;
		}
	
	
	//obtener una coneccion de la base de datos
		private Connection obtenerConexion() throws SQLException {
			return Conexion.getConnection();
		}
		

		//listar Provincias
		public List<DetParametrica> listarProvincias() throws SQLException {
			ResultSet resulSet = null ;
			List<DetParametrica> listaProvincias = new ArrayList<>();
			
			String sql = null ;				//consulta sql vacia
			estadoOperacion = false ;		// es false para decirle que no se ha realizado una consulta a la BD
			connection = obtenerConexion();	//obtiene la conecci
			
			try {
				
				sql = "SELECT * FROM det_parametrica WHERE CABP_ID='2'" ;
				statement = connection.prepareStatement(sql);
				resulSet = statement.executeQuery(sql);
				
				while (resulSet.next()) {
					DetParametrica detP = new DetParametrica();
					detP.setDETP_ID(resulSet.getInt(1));
					detP.setDETP_CODIGO(resulSet.getString(2));
					detP.setDETP_DESCRIPCION((resulSet.getString(3)));
					detP.setCABP_ID(resulSet.getInt(4));
					detP.setDETP_ESTADO(resulSet.getString(5));
					
					listaProvincias.add(detP);
				} 
				
			} catch (SQLException e) {
				e.printStackTrace();
			}
			
			return listaProvincias;
		}
		

		//listar SEXO
		public List<DetParametrica> listarSexo() throws SQLException {
			ResultSet resulSet = null ;
			List<DetParametrica> listaSexo = new ArrayList<>();
			
			String sql = null ;				//consulta sql vacia
			estadoOperacion = false ;		// es false para decirle que no se ha realizado una consulta a la BD
			connection = obtenerConexion();	//obtiene la conecci
			
			try {
				
				sql = "SELECT * FROM det_parametrica WHERE CABP_ID='1'" ;
				statement = connection.prepareStatement(sql);
				resulSet = statement.executeQuery(sql);
				
				while (resulSet.next()) {
					DetParametrica detP = new DetParametrica();
					detP.setDETP_ID(resulSet.getInt(1));
					detP.setDETP_CODIGO(resulSet.getString(2));
					detP.setDETP_DESCRIPCION((resulSet.getString(3)));
					detP.setCABP_ID(resulSet.getInt(4));
					detP.setDETP_ESTADO(resulSet.getString(5));
					
					listaSexo.add(detP);
				} 
				
			} catch (SQLException e) {
				e.printStackTrace();
			}
			
			return listaSexo;
		}
		
		//listar ESTADO CIVIL
				public List<DetParametrica> listarECivil() throws SQLException {
					ResultSet resulSet = null ;
					List<DetParametrica> listaCivil = new ArrayList<>();
					
					String sql = null ;				//consulta sql vacia
					estadoOperacion = false ;		// es false para decirle que no se ha realizado una consulta a la BD
					connection = obtenerConexion();	//obtiene la conecci
					
					try {
						
						sql = "SELECT * FROM det_parametrica WHERE CABP_ID='3'" ;
						statement = connection.prepareStatement(sql);
						resulSet = statement.executeQuery(sql);
						
						while (resulSet.next()) {
							DetParametrica detP = new DetParametrica();
							detP.setDETP_ID(resulSet.getInt(1));
							detP.setDETP_CODIGO(resulSet.getString(2));
							detP.setDETP_DESCRIPCION((resulSet.getString(3)));
							detP.setCABP_ID(resulSet.getInt(4));
							detP.setDETP_ESTADO(resulSet.getString(5));
							
							listaCivil.add(detP);
						} 
						
					} catch (SQLException e) {
						e.printStackTrace();
					}
					
					return listaCivil;
				}
				
	//listar DONANTES
	public List<Paciente> listarDonantes() throws SQLException {
		ResultSet resulSet = null ;
		List<Paciente> listaDonantes = new ArrayList<>();
		
		String sql = null ;				//consulta sql vacia
		estadoOperacion = false ;		// es false para decirle que no se ha realizado una consulta a la BD
		connection = obtenerConexion();	//obtiene la conecci
		
		try {			
			sql = "SELECT * FROM paciente where pa_tipo=1 and pa_seleccionado=0" ;
			statement = connection.prepareStatement(sql);
			resulSet = statement.executeQuery(sql);
			
			while (resulSet.next()) {
				Paciente pa1 = new Paciente();
				
				pa1.setPa_id(resulSet.getInt(1));
				pa1.setPa_cedula(resulSet.getString(2));
				pa1.setPa_extranjero(resulSet.getInt(3));
				pa1.setPa_nombres(resulSet.getString(4));
				pa1.setPa_apellidos(resulSet.getString(5));
				pa1.setPa_edad(resulSet.getInt(6));
				pa1.setPa_sexo(resulSet.getInt(7));
				pa1.setPa_estadoCivil(resulSet.getInt(8));
				pa1.setPa_provincia(resulSet.getInt(9));
				pa1.setPa_canton(resulSet.getString(10));
				pa1.setPa_direccion(resulSet.getString(11));
				pa1.setPa_email(resulSet.getString(12));
				pa1.setPa_tipo(resulSet.getInt(13));
				pa1.setPa_celular(resulSet.getString(14));
				pa1.setPa_checkAlergia(resulSet.getInt(15));
				pa1.setPa_descripcionAlergia(resulSet.getString(16));
				pa1.setPa_checkDiscapacidad(resulSet.getInt(17));
				pa1.setPa_descripcionDiscapacidad(resulSet.getString(18));
				pa1.setPa_certificado(resulSet.getBinaryStream(19));
				pa1.setPa_seleccionado(resulSet.getInt(20));
				pa1.setPa_estado(resulSet.getString(21));
				
				listaDonantes.add(pa1);
				}
					} catch (SQLException e) {
						e.printStackTrace();
					}
					return listaDonantes;
				}
	
	//listar CONTAGIADOS
		public List<Paciente> listarContagiados() throws SQLException {
			ResultSet resulSet = null ;
			List<Paciente> listaContagiados = new ArrayList<>();
			
			String sql = null ;				//consulta sql vacia
			estadoOperacion = false ;		// es false para decirle que no se ha realizado una consulta a la BD
			connection = obtenerConexion();	//obtiene la conecci
			
			try {			
				sql = "SELECT * FROM paciente where pa_tipo=2 and pa_seleccionado=0" ;
				statement = connection.prepareStatement(sql);
				resulSet = statement.executeQuery(sql);
				
				while (resulSet.next()) {
					Paciente pa1 = new Paciente();
					
					pa1.setPa_id(resulSet.getInt(1));
					pa1.setPa_cedula(resulSet.getString(2));
					pa1.setPa_extranjero(resulSet.getInt(3));
					pa1.setPa_nombres(resulSet.getString(4));
					pa1.setPa_apellidos(resulSet.getString(5));
					pa1.setPa_edad(resulSet.getInt(6));
					pa1.setPa_sexo(resulSet.getInt(7));
					pa1.setPa_estadoCivil(resulSet.getInt(8));
					pa1.setPa_provincia(resulSet.getInt(9));
					pa1.setPa_canton(resulSet.getString(10));
					pa1.setPa_direccion(resulSet.getString(11));
					pa1.setPa_email(resulSet.getString(12));
					pa1.setPa_tipo(resulSet.getInt(13));
					pa1.setPa_celular(resulSet.getString(14));
					pa1.setPa_checkAlergia(resulSet.getInt(15));
					pa1.setPa_descripcionAlergia(resulSet.getString(16));
					pa1.setPa_checkDiscapacidad(resulSet.getInt(17));
					pa1.setPa_descripcionDiscapacidad(resulSet.getString(18));
					pa1.setPa_certificado(resulSet.getBinaryStream(19));
					pa1.setPa_seleccionado(resulSet.getInt(20));
					pa1.setPa_estado(resulSet.getString(21));
					
					listaContagiados.add(pa1);
					}
						} catch (SQLException e) {
							e.printStackTrace();
						}
						return listaContagiados;
					}

		//SELECCIONAR PACIENTE
		public boolean seleccionarPaciente(Paciente paciente) throws SQLException {
			String sql = null ;
			estadoOperacion = false ;
			connection = obtenerConexion();
			
			try {
				connection.setAutoCommit(false);
				sql = "UPDATE paciente SET pa_seleccionado=? WHERE pa_id=?";
				statement = connection.prepareStatement(sql);
				
				statement.setInt(1, paciente.getPa_seleccionado());
				statement.setInt(2, paciente.getPa_id());
				
				estadoOperacion = statement.executeUpdate() > 0;
				connection.commit();
				statement.close();
				connection.close();
				
			} catch (SQLException e) {
				
				connection.rollback(); // regresar al estado anterior antes de dar el error.
				e.printStackTrace();
			}
			
			return estadoOperacion;
		}
		
//listar SELECCIONADOS
public List<Paciente> listarSeleccionados() throws SQLException {
	ResultSet resulSet = null ;
	List<Paciente> listaSeleccionados = new ArrayList<>();
					
	String sql = null ;
	estadoOperacion = false ;
	connection = obtenerConexion();
					
	try {			
		sql = "SELECT * FROM paciente WHERE pa_seleccionado=1 or pa_seleccionado=2" ;
		statement = connection.prepareStatement(sql);
		resulSet = statement.executeQuery(sql);
						
		while (resulSet.next()) {
			Paciente pa1 = new Paciente();
							
			pa1.setPa_id(resulSet.getInt(1));
			pa1.setPa_cedula(resulSet.getString(2));
			pa1.setPa_extranjero(resulSet.getInt(3));
			pa1.setPa_nombres(resulSet.getString(4));
			pa1.setPa_apellidos(resulSet.getString(5));
			pa1.setPa_edad(resulSet.getInt(6));
			pa1.setPa_sexo(resulSet.getInt(7));
			pa1.setPa_estadoCivil(resulSet.getInt(8));
			pa1.setPa_provincia(resulSet.getInt(9));
			pa1.setPa_canton(resulSet.getString(10));
			pa1.setPa_direccion(resulSet.getString(11));
			pa1.setPa_email(resulSet.getString(12));
			pa1.setPa_tipo(resulSet.getInt(13));
			pa1.setPa_celular(resulSet.getString(14));
			pa1.setPa_checkAlergia(resulSet.getInt(15));
			pa1.setPa_descripcionAlergia(resulSet.getString(16));
			pa1.setPa_checkDiscapacidad(resulSet.getInt(17));
			pa1.setPa_descripcionDiscapacidad(resulSet.getString(18));
			pa1.setPa_certificado(resulSet.getBinaryStream(19));
			pa1.setPa_seleccionado(resulSet.getInt(20));
			pa1.setPa_estado(resulSet.getString(21));
							
			listaSeleccionados.add(pa1);
			}
		} catch (SQLException e) {
			e.printStackTrace();
			}
	return listaSeleccionados;
	}
	
// obtener un solo paciente
public Paciente obtenerPaciente(int idPaciente) throws SQLException {
					
	ResultSet resulSet = null ;
	Paciente pac = new Paciente();
					
	String sql = null ;				//consulta sql vacia
	estadoOperacion = false ;		// es false para decirle que no se ha realizado una consulta a la BD
	connection = obtenerConexion();	//obtiene la conecci
					
	try {
						
		sql = "SELECT * FROM paciente WHERE pa_id=?" ;
		statement = connection.prepareStatement(sql);
		statement.setInt(1, idPaciente);
						
		resulSet = statement.executeQuery();
						
		if (resulSet.next()) {
			pac.setPa_id(resulSet.getInt(1));
			pac.setPa_cedula(resulSet.getString(2));
			pac.setPa_extranjero(resulSet.getInt(3));
			pac.setPa_nombres(resulSet.getString(4));
			pac.setPa_apellidos(resulSet.getString(5));
			pac.setPa_edad(resulSet.getInt(6));
			pac.setPa_sexo(resulSet.getInt(7));
			pac.setPa_estadoCivil(resulSet.getInt(8));
			pac.setPa_provincia(resulSet.getInt(9));
			pac.setPa_canton(resulSet.getString(10));
			pac.setPa_direccion(resulSet.getString(11));
			pac.setPa_email(resulSet.getString(12));
			pac.setPa_tipo(resulSet.getInt(13));
			pac.setPa_celular(resulSet.getString(14));
			pac.setPa_checkAlergia(resulSet.getInt(15));
			pac.setPa_descripcionAlergia(resulSet.getString(16));
			pac.setPa_checkDiscapacidad(resulSet.getInt(17));
			pac.setPa_descripcionDiscapacidad(resulSet.getString(18));
			pac.setPa_certificado(resulSet.getBinaryStream(19));
			pac.setPa_seleccionado(resulSet.getInt(20));
			pac.setPa_estado(resulSet.getString(21));
			}
						
		} catch (SQLException e) {
			e.printStackTrace();
		}
					
		return pac;		
		}

//METODO GUARDAr VALORACION MEDICA DE UN PACIENTE
	public boolean guardarValoracion(valoracionMedica valoracion) throws SQLException {
		String sql = null ;				//consulta sql vacia
		estadoOperacion = false ;		// es false para decirle que no se ha realizado una consulta a la BD
		connection = obtenerConexion();	//obtiene la coneccion
		
		try {
			
			connection.setAutoCommit(false);		//
			// consulta sql que se va a realizar
			sql = "INSERT INTO valoracionmedica (va_id, pa_id, va_diagnostico, va_tratamiento, va_receta, va_observaciones) VALUES (?,?,?,?,?,?)";
			statement = connection.prepareStatement(sql);			//prepara la sentencia SQL hacia la Base de datos
			
			statement.setString(1, null);			// el id es autoincremental, asi que es null(valor por defecto que se genera)
			statement.setInt(2, valoracion.getPa_id());
			statement.setString(3, valoracion.getVa_diagnostico());
			statement.setString(4, valoracion.getVa_tratamiento());
			statement.setString(5, valoracion.getVa_receta());
			statement.setString(6, valoracion.getVa_observaciones());
			
			estadoOperacion = statement.executeUpdate() > 0;			// significa que se realiz� la consulta exitosamente	
			
			connection.commit();
			statement.close();
			connection.close();
			
		} catch (SQLException e) {
			
			connection.rollback();		//rollback se usa cuando la consulta falla, y no se guardar , restablece antes del error
			e.printStackTrace();
		}
		
		return estadoOperacion;
	}
	
	// obtener HISTORIAL DE UN PACIENTE
	public List<valoracionMedica> obtenerHistorial(int idPaciente) throws SQLException {
						
		ResultSet resulSet = null ;
		List<valoracionMedica> listaHistorial = new ArrayList<>();
						
		String sql = null ;				//consulta sql vacia
		estadoOperacion = false ;		// es false para decirle que no se ha realizado una consulta a la BD
		connection = obtenerConexion();	//obtiene la conecci
						
		try {
							
			sql = "SELECT * FROM valoracionmedica WHERE pa_id=?" ;
			statement = connection.prepareStatement(sql);
			statement.setInt(1, idPaciente);
							
			resulSet = statement.executeQuery();
			
			while (resulSet.next()) {
				valoracionMedica val = new valoracionMedica();
				
				val.setVa_id(resulSet.getInt(1));
				val.setPa_id(resulSet.getInt(2));
				val.setVa_diagnostico(resulSet.getString(3));
				val.setVa_tratamiento(resulSet.getString(4));
				val.setVa_receta(resulSet.getString(5));
				val.setVa_observaciones(resulSet.getString(6));				
				
				listaHistorial.add(val);
			}
							
			} catch (SQLException e) {
				e.printStackTrace();
		}
						
		return listaHistorial;		
	}

	
	// obtener un pdf del paciente
	public byte[] obtenerPdf(int idPaciente) throws SQLException {
						
		ResultSet resulSet = null ;
		byte[] b = null;
						
		String sql = null ;				//consulta sql vacia
		estadoOperacion = false ;		// es false para decirle que no se ha realizado una consulta a la BD
		connection = obtenerConexion();	//obtiene la conecci
						
		try {
							
			sql = "SELECT pa_certificado FROM paciente WHERE pa_id=?" ;
			statement = connection.prepareStatement(sql);
			statement.setInt(1, idPaciente);
							
			resulSet = statement.executeQuery();
							
			 while (resulSet.next()) {
	                b = resulSet.getBytes(1);
	            }
					
			 
	            
			} catch (SQLException e) {
				e.printStackTrace();
			}
		return b;
						
				
			}
	

}

