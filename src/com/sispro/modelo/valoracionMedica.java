package com.sispro.modelo;

public class valoracionMedica {

	private int va_id;
	private int pa_id;
	private String va_diagnostico;
	private String va_tratamiento;
	private String va_receta;
	private String va_observaciones;
	
	public valoracionMedica() {
		super();
	}

	public valoracionMedica(int va_id, int pa_id, String va_diagnostico, String va_tratamiento, String va_receta,
			String va_observaciones) {
		super();
		this.va_id = va_id;
		this.pa_id = pa_id;
		this.va_diagnostico = va_diagnostico;
		this.va_tratamiento = va_tratamiento;
		this.va_receta = va_receta;
		this.va_observaciones = va_observaciones;
	}

	public int getVa_id() {
		return va_id;
	}

	public void setVa_id(int va_id) {
		this.va_id = va_id;
	}

	public int getPa_id() {
		return pa_id;
	}

	public void setPa_id(int pa_id) {
		this.pa_id = pa_id;
	}

	public String getVa_diagnostico() {
		return va_diagnostico;
	}

	public void setVa_diagnostico(String va_diagnostico) {
		this.va_diagnostico = va_diagnostico;
	}

	public String getVa_tratamiento() {
		return va_tratamiento;
	}

	public void setVa_tratamiento(String va_tratamiento) {
		this.va_tratamiento = va_tratamiento;
	}

	public String getVa_receta() {
		return va_receta;
	}

	public void setVa_receta(String va_receta) {
		this.va_receta = va_receta;
	}

	public String getVa_observaciones() {
		return va_observaciones;
	}

	public void setVa_observaciones(String va_observaciones) {
		this.va_observaciones = va_observaciones;
	}

	@Override
	public String toString() {
		return "valoracionMedica [va_id=" + va_id + ", pa_id=" + pa_id + ", va_diagnostico=" + va_diagnostico
				+ ", va_tratamiento=" + va_tratamiento + ", va_receta=" + va_receta + ", va_observaciones="
				+ va_observaciones + "]";
	}
	
	
}
