package com.sispro.modelo;

public class Usuario {
	
	private int usu_id ;
	private int per_id ;
	private String usu_cedula ;
	private String usu_nombres ;
	private String usu_apellidos ;
	private String usu_direccion ;
	private String usu_celular ;
	private String usu_email ;
	private String usu_usuario ;
	private String usu_clave ;
	private String usu_estado ;
	
	public Usuario() {
		super();
	}
	
	public Usuario(int usu_id, int per_id, String usu_cedula, String usu_nombres, String usu_apellidos,
			String usu_direccion, String usu_celular, String usu_email, String usu_usuario, String usu_clave,
			String usu_estado) {
		super();
		this.usu_id = usu_id;
		this.per_id = per_id;
		this.usu_cedula = usu_cedula;
		this.usu_nombres = usu_nombres;
		this.usu_apellidos = usu_apellidos;
		this.usu_direccion = usu_direccion;
		this.usu_celular = usu_celular;
		this.usu_email = usu_email;
		this.usu_usuario = usu_usuario;
		this.usu_clave = usu_clave;
		this.usu_estado = usu_estado;
	}


	public int getUsu_id() {
		return usu_id;
	}

	public void setUsu_id(int usu_id) {
		this.usu_id = usu_id;
	}

	public int getPer_id() {
		return per_id;
	}

	public void setPer_id(int per_id) {
		this.per_id = per_id;
	}

	public String getUsu_cedula() {
		return usu_cedula;
	}

	public void setUsu_cedula(String usu_cedula) {
		this.usu_cedula = usu_cedula;
	}

	public String getUsu_nombres() {
		return usu_nombres;
	}

	public void setUsu_nombres(String usu_nombres) {
		this.usu_nombres = usu_nombres;
	}

	public String getUsu_apellidos() {
		return usu_apellidos;
	}

	public void setUsu_apellidos(String usu_apellidos) {
		this.usu_apellidos = usu_apellidos;
	}

	public String getUsu_direccion() {
		return usu_direccion;
	}

	public void setUsu_direccion(String usu_direccion) {
		this.usu_direccion = usu_direccion;
	}

	public String getUsu_celular() {
		return usu_celular;
	}

	public void setUsu_celular(String usu_celular) {
		this.usu_celular = usu_celular;
	}

	public String getUsu_email() {
		return usu_email;
	}

	public void setUsu_email(String usu_email) {
		this.usu_email = usu_email;
	}

	public String getUsu_usuario() {
		return usu_usuario;
	}

	public void setUsu_usuario(String usu_usuario) {
		this.usu_usuario = usu_usuario;
	}

	public String getUsu_clave() {
		return usu_clave;
	}

	public void setUsu_clave(String usu_clave) {
		this.usu_clave = usu_clave;
	}

	public String getUsu_estado() {
		return usu_estado;
	}

	public void setUsu_estado(String usu_estado) {
		this.usu_estado = usu_estado;
	}

	@Override
	public String toString() {
		return "Usuario [usu_id=" + usu_id + ", per_id=" + per_id + ", usu_cedula=" + usu_cedula + ", usu_nombres="
				+ usu_nombres + ", usu_apellidos=" + usu_apellidos + ", usu_direccion=" + usu_direccion
				+ ", usu_celular=" + usu_celular + ", usu_email=" + usu_email + ", usu_usuario=" + usu_usuario
				+ ", usu_clave=" + usu_clave + ", usu_estado=" + usu_estado + "]";
	}
	
}
