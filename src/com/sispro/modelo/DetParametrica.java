package com.sispro.modelo;

public class DetParametrica {

	private int DETP_ID ;
	private String DETP_CODIGO ;
	private String DETP_DESCRIPCION ;
	private int CABP_ID ;
	private String DETP_ESTADO ;
	
	public DetParametrica() {
		super();
	}

	public DetParametrica(int dETP_ID, String dETP_CODIGO, String dETP_DESCRIPCION, int cABP_ID, String dETP_ESTADO) {
		super();
		DETP_ID = dETP_ID;
		DETP_CODIGO = dETP_CODIGO;
		DETP_DESCRIPCION = dETP_DESCRIPCION;
		CABP_ID = cABP_ID;
		DETP_ESTADO = dETP_ESTADO;
	}

	public int getDETP_ID() {
		return DETP_ID;
	}

	public void setDETP_ID(int dETP_ID) {
		DETP_ID = dETP_ID;
	}

	public String getDETP_CODIGO() {
		return DETP_CODIGO;
	}

	public void setDETP_CODIGO(String dETP_CODIGO) {
		DETP_CODIGO = dETP_CODIGO;
	}

	public String getDETP_DESCRIPCION() {
		return DETP_DESCRIPCION;
	}

	public void setDETP_DESCRIPCION(String dETP_DESCRIPCION) {
		DETP_DESCRIPCION = dETP_DESCRIPCION;
	}

	public int getCABP_ID() {
		return CABP_ID;
	}

	public void setCABP_ID(int cABP_ID) {
		CABP_ID = cABP_ID;
	}

	public String getDETP_ESTADO() {
		return DETP_ESTADO;
	}

	public void setDETP_ESTADO(String dETP_ESTADO) {
		DETP_ESTADO = dETP_ESTADO;
	}

	@Override
	public String toString() {
		return "DetParametrica [DETP_ID=" + DETP_ID + ", DETP_CODIGO=" + DETP_CODIGO + ", DETP_DESCRIPCION="
				+ DETP_DESCRIPCION + ", CABP_ID=" + CABP_ID + ", DETP_ESTADO=" + DETP_ESTADO + "]";
	}
	
	
}

