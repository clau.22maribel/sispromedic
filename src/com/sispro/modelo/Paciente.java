package com.sispro.modelo;

import java.io.InputStream;

public class Paciente {
	
	private int pa_id;
	private String pa_cedula;
	private int pa_extranjero;
	private String pa_nombres;
	private String pa_apellidos;
	private int pa_edad;
	private int pa_sexo;
	private int pa_estadoCivil;
	private int pa_provincia;
	private String pa_canton;
	private String pa_direccion;
	private String pa_email;
	private int pa_tipo;
	private String pa_celular;
	private int pa_checkAlergia;
	private String pa_descripcionAlergia;
	private int pa_checkDiscapacidad;
	private String pa_descripcionDiscapacidad;
	private InputStream pa_certificado;
	//private String pa_certificado;
	//private byte[] pa_certificado2;
	private int pa_seleccionado;
	private String pa_estado;
	
	public Paciente() {
		super();
	}

	/*
	public Paciente(int pa_id, String pa_cedula, int pa_extranjero, String pa_nombres, String pa_apellidos, int pa_edad,
			int pa_sexo, int pa_estadoCivil, int pa_provincia, String pa_canton, String pa_direccion, String pa_email,
			int pa_tipo, String pa_celular, int pa_checkAlergia, String pa_descripcionAlergia, int pa_checkDiscapacidad,
			String pa_descripcionDiscapacidad, byte[] pa_certificado2, int pa_seleccionado, String pa_estado) {
		super();
		this.pa_id = pa_id;
		this.pa_cedula = pa_cedula;
		this.pa_extranjero = pa_extranjero;
		this.pa_nombres = pa_nombres;
		this.pa_apellidos = pa_apellidos;
		this.pa_edad = pa_edad;
		this.pa_sexo = pa_sexo;
		this.pa_estadoCivil = pa_estadoCivil;
		this.pa_provincia = pa_provincia;
		this.pa_canton = pa_canton;
		this.pa_direccion = pa_direccion;
		this.pa_email = pa_email;
		this.pa_tipo = pa_tipo;
		this.pa_celular = pa_celular;
		this.pa_checkAlergia = pa_checkAlergia;
		this.pa_descripcionAlergia = pa_descripcionAlergia;
		this.pa_checkDiscapacidad = pa_checkDiscapacidad;
		this.pa_descripcionDiscapacidad = pa_descripcionDiscapacidad;
		this.pa_certificado2 = pa_certificado2;
		this.pa_seleccionado = pa_seleccionado;
		this.pa_estado = pa_estado;
	}

*/
	

	

	public Paciente(int pa_id, String pa_cedula, int pa_extranjero, String pa_nombres, String pa_apellidos, int pa_edad,
			int pa_sexo, int pa_estadoCivil, int pa_provincia, String pa_canton, String pa_direccion, String pa_email,
			int pa_tipo, String pa_celular, int pa_checkAlergia, String pa_descripcionAlergia, int pa_checkDiscapacidad,
			String pa_descripcionDiscapacidad, InputStream pa_certificado, int pa_seleccionado, String pa_estado) {
		super();
		this.pa_id = pa_id;
		this.pa_cedula = pa_cedula;
		this.pa_extranjero = pa_extranjero;
		this.pa_nombres = pa_nombres;
		this.pa_apellidos = pa_apellidos;
		this.pa_edad = pa_edad;
		this.pa_sexo = pa_sexo;
		this.pa_estadoCivil = pa_estadoCivil;
		this.pa_provincia = pa_provincia;
		this.pa_canton = pa_canton;
		this.pa_direccion = pa_direccion;
		this.pa_email = pa_email;
		this.pa_tipo = pa_tipo;
		this.pa_celular = pa_celular;
		this.pa_checkAlergia = pa_checkAlergia;
		this.pa_descripcionAlergia = pa_descripcionAlergia;
		this.pa_checkDiscapacidad = pa_checkDiscapacidad;
		this.pa_descripcionDiscapacidad = pa_descripcionDiscapacidad;
		this.pa_certificado = pa_certificado;
		this.pa_seleccionado = pa_seleccionado;
		this.pa_estado = pa_estado;
	}

	
	public int getPa_id() {
		return pa_id;
	}
	
	public void setPa_id(int pa_id) {
		this.pa_id = pa_id;
	}

	public String getPa_cedula() {
		return pa_cedula;
	}

	public void setPa_cedula(String pa_cedula) {
		this.pa_cedula = pa_cedula;
	}

	public int getPa_extranjero() {
		return pa_extranjero;
	}

	public void setPa_extranjero(int pa_extranjero) {
		this.pa_extranjero = pa_extranjero;
	}

	public String getPa_nombres() {
		return pa_nombres;
	}

	public void setPa_nombres(String pa_nombres) {
		this.pa_nombres = pa_nombres;
	}

	public String getPa_apellidos() {
		return pa_apellidos;
	}

	public void setPa_apellidos(String pa_apellidos) {
		this.pa_apellidos = pa_apellidos;
	}

	public int getPa_edad() {
		return pa_edad;
	}

	public void setPa_edad(int pa_edad) {
		this.pa_edad = pa_edad;
	}

	public int getPa_sexo() {
		return pa_sexo;
	}

	public void setPa_sexo(int pa_sexo) {
		this.pa_sexo = pa_sexo;
	}

	public int getPa_estadoCivil() {
		return pa_estadoCivil;
	}

	public void setPa_estadoCivil(int pa_estadoCivil) {
		this.pa_estadoCivil = pa_estadoCivil;
	}

	public int getPa_provincia() {
		return pa_provincia;
	}

	public void setPa_provincia(int pa_provincia) {
		this.pa_provincia = pa_provincia;
	}

	public String getPa_canton() {
		return pa_canton;
	}

	public void setPa_canton(String pa_canton) {
		this.pa_canton = pa_canton;
	}

	public String getPa_direccion() {
		return pa_direccion;
	}

	public void setPa_direccion(String pa_direccion) {
		this.pa_direccion = pa_direccion;
	}

	public String getPa_email() {
		return pa_email;
	}

	public void setPa_email(String pa_email) {
		this.pa_email = pa_email;
	}

	public int getPa_tipo() {
		return pa_tipo;
	}

	public void setPa_tipo(int pa_tipo) {
		this.pa_tipo = pa_tipo;
	}

	public String getPa_celular() {
		return pa_celular;
	}

	public void setPa_celular(String pa_celular) {
		this.pa_celular = pa_celular;
	}

	public int getPa_checkAlergia() {
		return pa_checkAlergia;
	}

	public void setPa_checkAlergia(int pa_checkAlergia) {
		this.pa_checkAlergia = pa_checkAlergia;
	}

	public String getPa_descripcionAlergia() {
		return pa_descripcionAlergia;
	}

	public void setPa_descripcionAlergia(String pa_descripcionAlergia) {
		this.pa_descripcionAlergia = pa_descripcionAlergia;
	}

	public int getPa_checkDiscapacidad() {
		return pa_checkDiscapacidad;
	}

	public void setPa_checkDiscapacidad(int pa_checkDiscapacidad) {
		this.pa_checkDiscapacidad = pa_checkDiscapacidad;
	}

	public String getPa_descripcionDiscapacidad() {
		return pa_descripcionDiscapacidad;
	}

	public void setPa_descripcionDiscapacidad(String pa_descripcionDiscapacidad) {
		this.pa_descripcionDiscapacidad = pa_descripcionDiscapacidad;
	}

	public InputStream getPa_certificado() {
		return pa_certificado;
	}

	public void setPa_certificado(InputStream pa_certificado) {
		this.pa_certificado = pa_certificado;
	}

	/*
	public byte[] getPa_certificado2() {
		return pa_certificado2;
	}

	public void setPa_certificado2(byte[] pa_certificado2) {
		this.pa_certificado2 = pa_certificado2;
	}*/

	public int getPa_seleccionado() {
		return pa_seleccionado;
	}

	public void setPa_seleccionado(int pa_seleccionado) {
		this.pa_seleccionado = pa_seleccionado;
	}

	public String getPa_estado() {
		return pa_estado;
	}

	public void setPa_estado(String pa_estado) {
		this.pa_estado = pa_estado;
	}

	@Override
	public String toString() {
		return "Paciente [pa_id=" + pa_id + ", pa_cedula=" + pa_cedula + ", pa_extranjero=" + pa_extranjero
				+ ", pa_nombres=" + pa_nombres + ", pa_apellidos=" + pa_apellidos + ", pa_edad=" + pa_edad
				+ ", pa_sexo=" + pa_sexo + ", pa_estadoCivil=" + pa_estadoCivil + ", pa_provincia=" + pa_provincia
				+ ", pa_canton=" + pa_canton + ", pa_direccion=" + pa_direccion + ", pa_email=" + pa_email
				+ ", pa_tipo=" + pa_tipo + ", pa_celular=" + pa_celular + ", pa_checkAlergia=" + pa_checkAlergia
				+ ", pa_descripcionAlergia=" + pa_descripcionAlergia + ", pa_checkDiscapacidad=" + pa_checkDiscapacidad
				+ ", pa_descripcionDiscapacidad=" + pa_descripcionDiscapacidad + ", pa_certificado=" + pa_certificado
				+ ", pa_seleccionado=" + pa_seleccionado + ", pa_estado=" + pa_estado + "]";
	}

}

