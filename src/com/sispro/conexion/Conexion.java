package com.sispro.conexion;

import java.sql.Connection;
import java.sql.SQLException;

import javax.sql.DataSource;

import org.apache.tomcat.dbcp.dbcp2.BasicDataSource;


public class Conexion {
	//objeto de tipo datasource que empiece vacion(null)
	//es estatico xq solo sera accedido dentro de la clase conexion
	private static BasicDataSource dataSource = null;
	
	//metodo de tipo datasource 
	//DataSource es un nombre dado a la conexi�n configurada a una base de datos desde un servidor.
	//El nombre se usa com�nmente al crear una consulta a la base de datos.
	private static DataSource getDataSource() {
		//preguntamos si no se ha realizado una conexion
		if (dataSource == null) {
			dataSource = new BasicDataSource();
			//llamamos al driver jdbc para la conexion
			dataSource.setDriverClassName("com.mysql.cj.jdbc.Driver");
			//usuario de la base de datos
			dataSource.setUsername("");
			//clave de la base de datos
			dataSource.setPassword("");
			//direccion o url, cadena que contiene el nombre del servidor, puerto, nombre de la base de dato y zona horaria
			dataSource.setUrl("jdbc:mysql://localhost:3306/dbsispromedic?serverTimezone=UTC");
			dataSource.setInitialSize(20);		// cuantas conecciones iniciaremos 20
			dataSource.setMaxIdle(15);			// conexiones activas 15
			dataSource.setMaxTotal(20);			// conexiones totales 20
			dataSource.setMaxWaitMillis(50000);	// tiempo que debe esperar una nueva conexion 5milisegundos
		}
		
		return dataSource;  //retornamos
	}
	
	//retornamos toda la conexion 
	public static Connection getConnection() throws SQLException {
		return getDataSource().getConnection();
	}

}
