<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css" integrity="sha384-9aIt2nRpC12Uk9gS9baDl411NQApFmC26EwAOH8WgZl5MYYxFfc+NcPb1dKGj7Sk" crossorigin="anonymous">
    
    <link href="Css/styles.css" rel="stylesheet">
    
    <link href="https://fonts.googleapis.com/css2?family=Roboto&family=Titillium+Web:wght@300;400;600&display=swap" rel="stylesheet">
    <script src="https://unpkg.com/ionicons@5.0.0/dist/ionicons.js"></script>

    <title>SISPROMEDIC</title>
  </head>
  <body>
    
    <div id="principal">
   <nav class="navbar navbar-expand-lg">
   	<div class="container">
  	<a class="navbar-brand" href="#"><img src="images/logoPrincipal.ico" class="logoPro" alt="logo"> SISPROMEDIC</a>
  		<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
    		<span class="navbar-toggler-icon"></span>
  		</button>

  <div class="collapse navbar-collapse" id="navbarSupportedContent">
    <ul class="navbar-nav ml-auto">
      <li class="nav-item">
        <a class="nav-link" href="#">Home </a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="paciente?opcion=donante">Donantes</a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="paciente?opcion=contagiado">Contagiados</a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="login.jsp">Iniciar Sesion</a>
      </li>
    </ul>
  </div>
  </div>
</nav>
</div>
	<div class="container">
		<div id="carouselExampleControls" class="carousel slide" data-ride="carousel">
  		<div class="carousel-inner">
    		<div class="carousel-item active">
      			<img src="images/banner1.jpg" class="d-block w-100" alt="...">
    		</div>
    		<div class="carousel-item">
      			<img src="images/banner2.png" class="d-block w-100" alt="...">
    		</div>
    		<div class="carousel-item">
      			<img src="images/banner1.jpg" class="d-block w-100" alt="...">
    		</div>
  		</div>
  		<a class="carousel-control-prev" href="#carouselExampleControls" role="button" data-slide="prev">
    		<span class="carousel-control-prev-icon" aria-hidden="true"></span>
    		<span class="sr-only">Anterior</span>
  		</a>
  		<a class="carousel-control-next" href="#carouselExampleControls" role="button" data-slide="next">
    		<span class="carousel-control-next-icon" aria-hidden="true"></span>
    		<span class="sr-only">Siguiente</span>
  		</a>
		</div>
		</div>

	<br>
	<br>
	
	<section >
		<div class="container"> ENLACES DE INTERES
  			<div class="row">
    			<div class="col">
      				<a href="https://www.salud.gob.ec/" target="_blank"><img src="images/link1.jpg" title="pdf" class="links"/></a>
    			</div>
    			<div class="col">
      				<a href="https://educacion.gob.ec/" target="_blank"><img src="images/link2.jpg" title="pdf" class="links"/></a>
    			</div>
    			<div class="col">
      				<a href="http://www.trabajo.gob.ec/" target="_blank"><img src="images/link3.jpg" title="pdf" class="links"/></a>
    			</div>
    			<div class="col">
      				<a href="https://www.sri.gob.ec/" target="_blank"><img src="images/link4.jpg" title="pdf" class="links"/></a>
    			</div>
    			<div class="col">
      				<a href="https://www.telecomunicaciones.gob.ec/" target="_blank"><img src="images/link5.jpg" title="pdf" class="links"/></a>
    			</div>
    			<div class="col">
      				<a href="https://www.inclusion.gob.ec/" target="_blank"><img src="images/link6.jpg" title="pdf" class="links"/></a>
    			</div>
  			</div>
		</div>
	</section>
	
	<br>
	<br>
	<br>
	<br>


    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/js/bootstrap.min.js" integrity="sha384-OgVRvuATP1z7JjHLkuOU7Xw704+h835Lr+6QL9UvYjZE3Ipu6Tp75j7Bh/kR0JKI" crossorigin="anonymous"></script>
  </body>
</html>