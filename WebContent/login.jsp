<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css" integrity="sha384-9aIt2nRpC12Uk9gS9baDl411NQApFmC26EwAOH8WgZl5MYYxFfc+NcPb1dKGj7Sk" crossorigin="anonymous">
    
    <link href="Css/styles.css" rel="stylesheet">
    
    <link href="https://fonts.googleapis.com/css2?family=Roboto&family=Titillium+Web:wght@300;400;600&display=swap" rel="stylesheet">
    <script src="https://unpkg.com/ionicons@5.0.0/dist/ionicons.js"></script>

    <title>SISPROMEDIC</title>
  </head>
  <body>
    
    <div id="principal">
   <nav class="navbar navbar-expand-lg">
   	<div class="container">
  	<a class="navbar-brand" href="#"><img src="images/logoPrincipal.ico" class="logoPro" alt="logo"> SISPROMEDIC</a>
  		<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
    		<span class="navbar-toggler-icon"></span>
  		</button>

  <div class="collapse navbar-collapse" id="navbarSupportedContent">
    <ul class="navbar-nav ml-auto">
      <li class="nav-item">
        <a class="nav-link" href="index.jsp">Home </a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="paciente?opcion=donante">Donantes</a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="paciente?opcion=contagiado">Contagiados</a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="login.jsp">Iniciar Sesion</a>
      </li>
    </ul>
  </div>
  </div>
</nav>
</div>

<br>
<form action="persona" method="post">
<input type="hidden" name="opcion" value="autenticarUsuario">
	<div class="app-body">
  	<main class="main d-flex align-items-center">
    <div class="container">
      <div class="row">
        <div class="col-md-8 mx-auto">
          <div class="card-group">
            <div class="card p-4">
              <div class="card-body">
                <form>
                  <h1>Iniciar sesi�n</h1>
                  <p class="text-muted">Iniciar sesi�n en su cuenta</p>
                  <div class="input-group mb-3">
                    <div class="input-group-prepend">
                      <span class="input-group-text"><ion-icon name="person-circle-outline"></ion-icon></span>
                    </div>
                    	<input type="text" class="form-control" name="txtUsuario">
                  </div>
                  <div class="input-group mb-4">
                    <div class="input-group-prepend">
                      <span class="input-group-text"><ion-icon name="key-outline"></ion-icon></span>
                      
                    </div>
                    <input type="text" class="form-control" name="txtClave">
                    <!-- <input type="password" name="claveAuxiliar" placeholder="Password" autocomplete="current-password" required> -->
                  </div>
                  <div class="row">
                    <div class="col-6">
                
                       <!-- <a href="persona?opcion=crear">Iniciar Sesion</a>
                       <a href="persona?opcion=crear">Crear Usuario</a>
                       <a href="persona?opcion=listar">Listar Usuarios</a> -->
                      <!-- <button mat-raised-button class="btn btn-primary px-2" type="submit" href="persona?opcion=listar">Iniciar sesi�n</button>
                       -->
                       <button type="submit" class="btn btn-secondary" value="autenticarUsuario"> ENTRAR</button>
                    </div>
                  </div>
                </form>
              </div>
            </div>
            <div class="card text-white bg-primary py-5 d-md-down-none" style="width:44%">
              <div class="card-body text-center">
                  <img src="images/link1.jpg" class="img-avatar" style="width:90%"/>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </main>
</div>
	</form>
	
	<br>
	<br>


    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/js/bootstrap.min.js" integrity="sha384-OgVRvuATP1z7JjHLkuOU7Xw704+h835Lr+6QL9UvYjZE3Ipu6Tp75j7Bh/kR0JKI" crossorigin="anonymous"></script>
  </body>
</html>