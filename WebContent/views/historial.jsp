<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
    <%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css" integrity="sha384-9aIt2nRpC12Uk9gS9baDl411NQApFmC26EwAOH8WgZl5MYYxFfc+NcPb1dKGj7Sk" crossorigin="anonymous">
    
    <link href="Css/styles.css" rel="stylesheet">
    
    <link href="https://fonts.googleapis.com/css2?family=Roboto&family=Titillium+Web:wght@300;400;600&display=swap" rel="stylesheet">
    <script src="https://unpkg.com/ionicons@5.0.0/dist/ionicons.js"></script>

    <title>SISPROMEDIC</title>
  </head>
  <body>
    
    <div id="principal">
   <nav class="navbar navbar-expand-lg">
   	<div class="container">
  	<a class="navbar-brand" href="#"><img src="images/logoPrincipal.ico" class="logoPro" alt="logo"> SISPROMEDIC</a>
  		<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
    		<span class="navbar-toggler-icon"></span>
  		</button>

  <div class="collapse navbar-collapse" id="navbarSupportedContent">
    <ul class="navbar-nav ml-auto">
      <li class="nav-item">
        <a class="nav-link" href="login.jsp">Cerrar Sesion</a>
      </li>
    </ul>
  </div>
  </div>
</nav>
</div>

<br>
<br>
<h2 align="center" class="titulo">HISTORIAL CLINICO</h2>
<div class="container">
<c:set var="Listpaciente" value="${listarPaciente}"></c:set>
<div class="form-group row">
    	<label for="staticEmail" class="col-sm-2 col-form-label">CEDULA:</label>
    	<div class="col-sm-10">
      		<input type="text" readonly class="form-control-plaintext" id="staticEmail" value="${Listpaciente.pa_cedula}">
    	</div>
  	</div>
  	
  	<div class="form-group row">
    	<label for="staticEmail" class="col-sm-2 col-form-label">NOMBRES:</label>
    	<div class="col-sm-10">
      		<input type="text" readonly class="form-control-plaintext" id="staticEmail" value="${Listpaciente.pa_nombres}">
    	</div>
  	</div>
  	
  	<div class="form-group row">
    	<label for="staticEmail" class="col-sm-2 col-form-label">APELLIDOS:</label>
    	<div class="col-sm-10">
      		<input type="text" readonly class="form-control-plaintext" id="staticEmail" value="${Listpaciente.pa_apellidos}">
    	</div>
  	</div>
  	<br>
  	<br>

	<table class="table">
  		<thead class="thead-dark">
    		<tr>
      			<th scope="col">#</th>
      			<th scope="col">DIAGNOSTICO</th>
      			<th scope="col">TRATAMIENTO</th>
      			<th scope="col">RECETA M.</th>
      			<th scope="col">OBSERVACIONES</th>
    		</tr>
  		</thead>
  		<tbody>
  			<c:forEach var="listarHist" items="${listarHistorial}">
    		<tr>
      			<th scope="row"> <c:out value="${listarHist.va_id }"></c:out> </th>
      			<td> <c:out value="${listarHist.va_diagnostico }"></c:out> </td>
      			<td> <c:out value="${listarHist.va_tratamiento }"></c:out> </td>
      			<td> <c:out value="${listarHist.va_receta }"></c:out> </td>
      			<td> <c:out value="${listarHist.va_observaciones }"></c:out> </td>
    		</tr>
    		</c:forEach>
  		</tbody>
	</table>
	<a class="btn btn-secondary" href="paciente?opcion=ListaSeleccionados" role="button">REGRESAR</a>
</div>
<br>
<br>



    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/js/bootstrap.min.js" integrity="sha384-OgVRvuATP1z7JjHLkuOU7Xw704+h835Lr+6QL9UvYjZE3Ipu6Tp75j7Bh/kR0JKI" crossorigin="anonymous"></script>
  </body>
</html>