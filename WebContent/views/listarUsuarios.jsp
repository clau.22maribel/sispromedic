<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
    <%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css" integrity="sha384-9aIt2nRpC12Uk9gS9baDl411NQApFmC26EwAOH8WgZl5MYYxFfc+NcPb1dKGj7Sk" crossorigin="anonymous">
    
    <link href="Css/styles.css" rel="stylesheet">
    
    <link href="https://fonts.googleapis.com/css2?family=Roboto&family=Titillium+Web:wght@300;400;600&display=swap" rel="stylesheet">
    <script src="https://unpkg.com/ionicons@5.0.0/dist/ionicons.js"></script>

    <title>SISPROMEDIC</title>
  </head>
  <body>
    
    <div id="principal">
   <nav class="navbar navbar-expand-lg">
   	<div class="container">
  	<a class="navbar-brand" href="#"><img src="images/logoPrincipal.ico" class="logoPro" alt="logo"> SISPROMEDIC</a>
  		<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
    		<span class="navbar-toggler-icon"></span>
  		</button>

  <div class="collapse navbar-collapse" id="navbarSupportedContent">
    <ul class="navbar-nav ml-auto">
      <li class="nav-item">
        <a class="nav-link" href="login.jsp">Cerrar Sesion</a>
      </li>
    </ul>
  </div>
  </div>
</nav>
</div>

<br>
<br>
<h2 align="center" class="titulo">LISTADO DE USUARIO</h2>
<div class="container">
	<a href="persona?opcion=crear">Crear Usuario</a>
	<table class="table">
  		<thead class="thead-dark">
    		<tr>
      			<th scope="col">#</th>
      			<th scope="col">Cedula</th>
      			<th scope="col">Nombres</th>
      			<th scope="col">Apellidos</th>
      			<th scope="col">Perfil</th>
      			<th scope="col">Estado</th>
      			<th scope="col">Acciones</th>
    		</tr>
  		</thead>
  		<tbody>
  			<c:forEach var="listarUsu" items="${listarUsuarios}">
    		<tr>
      			<th scope="row"> <c:out value="${listarUsu.usu_id }"></c:out> </th>
      			<td> <c:out value="${listarUsu.usu_cedula }"></c:out> </td>
      			<td> <c:out value="${listarUsu.usu_nombres }"></c:out> </td>
      			<td> <c:out value="${listarUsu.usu_apellidos }"></c:out> </td>
      			<td> <c:out value="${listarUsu.per_id }"></c:out> </td>
      			<td> <c:out value="${listarUsu.usu_estado }"></c:out> </td>
      			<td>
      				<div> 
      					<a class="btn btn-primary" href="persona?opcion=meditar&usu_id=<c:out value="${listarUsu.usu_id }"></c:out>" role="button">Editar</a>
      					<a class="btn btn-primary" href="persona?opcion=eliminarUsu&usu_id=<c:out value="${listarUsu.usu_id }"></c:out>" role="button">Eliminar</a>
      				 </div>
      			</td>
    		</tr>
    		</c:forEach>
  		</tbody>
	</table>
</div>
<br>
<h6> PERFIL ID:</h6>
<h6> 1:ADMINISTRADOR	2:MEDICO</h6>

<br>
<br>


    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/js/bootstrap.min.js" integrity="sha384-OgVRvuATP1z7JjHLkuOU7Xw704+h835Lr+6QL9UvYjZE3Ipu6Tp75j7Bh/kR0JKI" crossorigin="anonymous"></script>
  </body>
</html>