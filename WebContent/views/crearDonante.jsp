<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
    <%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css" 
    integrity="sha384-9aIt2nRpC12Uk9gS9baDl411NQApFmC26EwAOH8WgZl5MYYxFfc+NcPb1dKGj7Sk" 
    crossorigin="anonymous">
    
    <link href="Css/styles.css" rel="stylesheet">
    
    <link href="https://fonts.googleapis.com/css2?family=Roboto&family=Titillium+Web:wght@300;400;600&display=swap" rel="stylesheet">
    <script src="https://unpkg.com/ionicons@5.0.0/dist/ionicons.js"></script>

    <title>SISPROMEDIC</title>
  </head>
  <body>
    
    <div id="principal">
   <nav class="navbar navbar-expand-lg">
   	<div class="container">
  	<a class="navbar-brand" href="#"><img src="images/logoPrincipal.ico" class="logoPro" alt="logo"> SISPROMEDIC</a>
  		<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
    		<span class="navbar-toggler-icon"></span>
  		</button>

  <div class="collapse navbar-collapse" id="navbarSupportedContent">
    <ul class="navbar-nav ml-auto">
       <li class="nav-item">
        <a class="nav-link" href="index.jsp">Home </a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="paciente?opcion=donante">Donantes</a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="paciente?opcion=contagiado">Contagiados</a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="login.jsp">Iniciar Sesion</a>
      </li>
    </ul>
  </div>
  </div>
</nav>
</div>

<br>
<br>
<h2 align="center" class="titulo">REGISTRO PARA DONANTE DE PLASMA CON ANTICUERPOS</h2>
<form action="paciente" method="post" enctype="multipart/form-data">
<input type="hidden" name="opcion" value="guardarDonante">
	
	<div class="container"> 
	<div class="row">
    <div class="col">
    
    	<div class="form-group">
    		<label >CEDULA *</label>
       		<input type="text" class="form-control" name="txtCedula">
       		
       		<div class="form-check">
  			<input class="form-check-input" type="checkbox" value="ON" name="checkExtranjero">
  			<label class="form-check-label"> EXTRANJERO </label>
			</div>
    	</div>
    		
    	<div class="form-group">
    		<label >NOMBRES *</label>
			<input type="text" class="form-control" name="txtNombres">
    	</div>
    		
    	<div class="form-group">
    		<label >APELLIDOS*</label>
			<input type="text" class="form-control" name="txtApellidos">
    	</div>
    		
    	<div class="form-group">
    		<label >EDAD</label>
			<input type="text" class="form-control" name="txtEdad">
    	</div>
    		
    	<div class="form-group">
    		<label >PROVINCIA</label>
			<select class="form-control" name="comboProvincia">
				<c:forEach var="provincia" items="${listarProvincias}">
					<option value="${provincia.DETP_ID }" > <c:out value="${provincia.DETP_DESCRIPCION }"></c:out> </option>
				</c:forEach>
			</select>
    	</div>
    	
    	<div class="form-group">
    		<label >CANTON*</label>
			<input type="text" class="form-control" name="txtCanton">
    	</div>
    
    	<div class="form-group">
    		<label >DIRECCION*</label>
			<input type="text" class="form-control" name="txtDireccion">
    	</div>
    </div>
    
    <div class="col">
    	
    	<div class="form-group">
    		<label >E-MAIL</label>
			<input type="text" class="form-control" name="txtEmail" placeholder="name@example.com">
    	</div>	
    	
    	<br>
    	
    	<div class="form-group">
    		<label >CELULAR</label>
			<input type="text" class="form-control" name="txtCelular" placeholder="099999999">
    	</div>
    	
    	<div class="form-group">
    		<label >SEXO</label>
			<select class="form-control" name="comboSexo">
				<c:forEach var="sexo" items="${listarSexo}">
					<option value="${sexo.DETP_ID }" > <c:out value="${sexo.DETP_DESCRIPCION }"></c:out> </option>
				</c:forEach>
			</select>
    	</div>
    	
    	<div class="form-group">
    		<label >ESTADO CIVIL</label>
			<select class="form-control" name="comboCivil">
				<c:forEach var="civil" items="${listarCivil}">
					<option value="${civil.DETP_ID }" > <c:out value="${civil.DETP_DESCRIPCION }"></c:out> </option>
				</c:forEach>
			</select>
    	</div>
    	
    	<div class="form-group">
    		<label >�ES ALERGICO A UN MEDICAMENTO?</label>
			<div class="form-check form-check-inline">
  				<input class="form-check-input" type="radio" name="alergico" value="1" onclick="txtMedicamento.disabled = false">
  				<label class="form-check-label" >SI</label>
			</div>
			<div class="form-check form-check-inline">
  				<input class="form-check-input" type="radio" name="alergico" value="0" onclick="txtMedicamento.disabled = true">
  				<label class="form-check-label" >NO</label>
			</div>
			<input type="text" class="form-control" name="txtMedicamento" disabled="disabled">
    	</div>
    	
    	<div class="form-group">
    		<label >�TIENE ALGUNA DISCAPACIDAD?</label>
			<div class="form-check form-check-inline">
  				<input class="form-check-input" type="radio" name="discapacidad" value="1" onclick="txtDiscapacidad.disabled = false">
  				<label class="form-check-label" >SI</label>
			</div>
			<div class="form-check form-check-inline">
  				<input class="form-check-input" type="radio" name="discapacidad" value="0" onclick="txtDiscapacidad.disabled = true">
  				<label class="form-check-label" >NO</label>
			</div>
			<input type="text" class="form-control" name="txtDiscapacidad" disabled="disabled">
    	</div>
    	
    	<div class="form-group">
    		<label for="exampleFormControlFile1">Examen Negativo Covid-19</label>
    		<input type="file" class="form-control-file" name="archivoCovid">
  		</div>
    </div>
  </div>
  </div>
  
	<br>
	<br>
	<div class="row">
	<div class="col-sm-2"></div>
	<div class="col-sm-4"></div>
	<div class="col-sm-4" style="text-align: right">
		<a class="btn btn-primary" href="index.jsp" role="button">CANCELAR</a>
		<button type="submit" class="btn btn-secondary" value="Guardar Usuario"> GUARDAR</button>
	</div>
	</div>
</form>
	<br>
	<br>


    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/js/bootstrap.min.js" integrity="sha384-OgVRvuATP1z7JjHLkuOU7Xw704+h835Lr+6QL9UvYjZE3Ipu6Tp75j7Bh/kR0JKI" crossorigin="anonymous"></script>
  </body>
</html>