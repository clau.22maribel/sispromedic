<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
    <%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css" integrity="sha384-9aIt2nRpC12Uk9gS9baDl411NQApFmC26EwAOH8WgZl5MYYxFfc+NcPb1dKGj7Sk" crossorigin="anonymous">
    
    <link href="Css/styles.css" rel="stylesheet">
    
    <link href="https://fonts.googleapis.com/css2?family=Roboto&family=Titillium+Web:wght@300;400;600&display=swap" rel="stylesheet">
    <script src="https://unpkg.com/ionicons@5.0.0/dist/ionicons.js"></script>

    <title>SISPROMEDIC</title>
  </head>
  <body>
    
    <div id="principal">
   <nav class="navbar navbar-expand-lg">
   	<div class="container">
  	<a class="navbar-brand" href="#"><img src="images/logoPrincipal.ico" class="logoPro" alt="logo"> SISPROMEDIC</a>
  		<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
    		<span class="navbar-toggler-icon"></span>
  		</button>

  <div class="collapse navbar-collapse" id="navbarSupportedContent">
    <ul class="navbar-nav ml-auto">
      <li class="nav-item">
        <a class="nav-link" href="login.jsp">Cerrar Sesion</a>
      </li>
    </ul>
  </div>
  </div>
</nav>
</div>

<br>
<br>
<h2 align="center" class="titulo">LISTADO DE DONANTES</h2>

<div class="container">
	<button type="button" class="btn btn-primary">Donantes</button>
	<a class="btn btn-secondary" href="paciente?opcion=ListaContagiados" role="button">Contagiados</a>
	<a class="btn btn-secondary" href="paciente?opcion=ListaSeleccionados" role="button">Seleccionados</a>
	<table class="table">
  		<thead class="thead-dark">
    		<tr>
      			<th scope="col">#</th>
      			<th scope="col">Cedula</th>
      			<th scope="col">Nombres</th>
      			<th scope="col">Apellidos</th>
      			<th scope="col">Edad</th>
      			<th scope="col">Provincia</th>
      			<th scope="col">Canton</th>
      			<th scope="col">Celular</th>
      			<th scope="col">Prueba Covid</th>
    		</tr>
  		</thead>
  		<tbody>
  			<c:forEach var="listarDona" items="${listarDonantes}">
    		<tr>
      			<th scope="row"> <c:out value="${listarDona.pa_id }"></c:out> </th>
      			<td> <c:out value="${listarDona.pa_cedula }"></c:out> </td>
      			<td> <c:out value="${listarDona.pa_nombres }"></c:out> </td>
      			<td> <c:out value="${listarDona.pa_apellidos }"></c:out> </td>
      			<td> <c:out value="${listarDona.pa_edad }"></c:out> </td>
      			<td> <c:out value="${listarDona.pa_provincia }"></c:out> </td>
      			<td> <c:out value="${listarDona.pa_canton }"></c:out> </td>
      			<td> <c:out value="${listarDona.pa_celular }"></c:out> </td>
      			<td> 
      			<c:if test="${listarDona.pa_certificado != null}">
 					<a href="paciente?opcion=archivoPdf&pa_id=<c:out value="${listarDona.pa_id }"></c:out>" target="_blank"><img src="images/mpdf.png" title="pdf"/></a>
				</c:if>
      			</td>
    		</tr>
    		</c:forEach>
  		</tbody>
	</table>
</div>
<br>
<h6> PROVINCIAS ID:</h6>
<h6> 3:ESMERALDA	4:MANABI	5:LOS RIOS	6:SANTA ELENA	7:GUAYAS	8:STO. DOMINGO	9:EL ORO </h6>
<h6> 10:AZUAY	11:BOLIVAR	12:CA�AR	13:CARCHI	14:COTOPAXI-	15:CHIMBORAZO	16:IMBABURA	17:LOJA </h6>
<h6> 18:PICHINCHA	19:TUNGURAHUA	20:MORONA SANTIAGO	21:NAPO	22:ORELLANA-	23:PASTAZA	24:SUCUMBIOS</h6>
<h6> 25:ZAMORA CHIPIPE	26:GALAPAGOS</h6>
<br>
<br>



    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/js/bootstrap.min.js" integrity="sha384-OgVRvuATP1z7JjHLkuOU7Xw704+h835Lr+6QL9UvYjZE3Ipu6Tp75j7Bh/kR0JKI" crossorigin="anonymous"></script>
  </body>
</html>